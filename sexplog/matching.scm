(define (pattern-variable? sym)
  (and (symbol? sym) 
       (eq? #\? (string-ref (symbol->string sym) 0))))

(define (wildcard? sym) (eq? sym '?_))

(define (variable-binding frame variable)
  (if (null? frame) frame 
    (if (eq? variable (caar frame))
      (cdar frame) 
      (variable-binding (cdr frame) variable))))

(define (bind-variable frame variable value)
  (cons (cons variable value) frame))

(define (unbind-variable frame variable)
  (filter
    (lambda (binding) (not (eq? variable (car binding))))
    frame))

(define (simple-match pattern datum frame)
  (cond ((null? pattern) (if (null? datum) frame #f))
        ((wildcard? pattern) frame)
        ((pattern-variable? pattern)
         (let ((binding (variable-binding frame pattern)))
           (if (null? binding)
             (bind-variable frame pattern datum)
             (if (equal? binding datum)
               frame #f))))
        ((pair? pattern)
         (if (pair? datum)
           (let ((next-frame (simple-match 
                               (car pattern) (car datum) frame)))
             (if next-frame 
               (simple-match (cdr pattern) (cdr datum) next-frame) #f)) #f))
        (else (if (equal? pattern datum) frame #f))))

(define (unify pattern1 pattern2 frame)
  (cond ((or (wildcard? pattern1) (wildcard? pattern2)) frame)
        ((and (pattern-variable? pattern1) (pattern-variable? pattern2))
         (let ((binding1 (variable-binding frame pattern1))
               (binding2 (variable-binding frame pattern2)))
           (cond ((and (null? binding1) (null? binding2)) 
                  (if (eq? pattern1 pattern2)
                    frame
                    (bind-variable frame pattern2 pattern1)))
                 ((null? binding1) (bind-variable frame pattern1 binding2))
                 ((null? binding2) (bind-variable frame pattern2 binding1))
                 (else (if (equal? binding1 binding2) frame #f)))))
        ((pattern-variable? pattern1) (simple-match pattern1 pattern2 frame))
        ((pattern-variable? pattern2) (simple-match pattern2 pattern1 frame))
        ((pair? pattern1)
         (if (pair? pattern2)
           (let ((next-frame (unify (car pattern1) (car pattern2) frame)))
             (if next-frame 
               (unify (cdr pattern1) (cdr pattern2) next-frame) #f)) #f))
        (else (if (equal? pattern1 pattern2) frame #f))))
