(define (rule-statement? stmt)
  (and (list? stmt) (eq? 'rule (car stmt))))

(define (substitute pattern frame)
  (cond ((or (null? pattern) (wildcard? pattern)) pattern)
        ((pair? pattern) 
         (cons (substitute (car pattern) frame)
               (substitute (cdr pattern) frame)))
        ((pattern-variable? pattern)
         (let ((binding (variable-binding frame pattern)))
           (if (null? binding) pattern binding)))
        (else pattern)))

(define (find-pattern-variables pattern variables)
  (cond ((null? pattern) variables)
        ((pattern-variable? pattern) (cons pattern variables))
        ((pair? pattern)
         (find-pattern-variables (cdr pattern)
            (find-pattern-variables (car pattern) variables)))
        (else variables)))

(define (gen-pattern-variables n)
  (if (= n 0) '()
    (cons (string->symbol 
            (string-append "?_" (symbol->string (gensym))))
          (gen-pattern-variables (- n 1)))))

(define (add-rule rulestmt rules)
  (let* ((conclusion (cadr rulestmt))
         (name (car conclusion))
         (args (cdr conclusion))
         (variables (find-pattern-variables args '()))
         (uniqvars (gen-pattern-variables (length variables)))
         (subtable (map cons variables uniqvars))
         (body (if (null? (cddr rulestmt)) '() (caddr rulestmt)))
         (new-conclusion (cons name (substitute args subtable)))
         (new-body (substitute body subtable)))
    (cons (cons new-conclusion new-body) rules)))

(define (find-rule conclusion rules frame)
  (if (null? rules) #f
    (let* ((query (cons conclusion '?_rule_body))
           (new-frame (unify query (car rules) frame)))
      (if new-frame new-frame
        (find-rule conclusion (cdr rules) frame)))))

(define (all-rules conclusion rules frames)
  (if (null? frames)
    (let ((rule (find-rule conclusion rules '())))
      (if rule (list rule) '()))
    (filter values
      (map (lambda (frame) 
             (find-rule conclusion rules frame))
           frames))))

(define (frame->body frame)
  (let ((body (variable-binding frame '?_rule_body))
        (new-frame (unbind-variable frame '?_rule_body)))
    (if (null? body) '?_
      (substitute body new-frame))))
