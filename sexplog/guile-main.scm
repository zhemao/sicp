#!/usr/bin/guile -s
!#

(load "mainloop.scm")

(if (null? (cdr (command-line))) '()
  (let* ((dbfile (cadr (command-line)))
         (db (read-database dbfile)))
    (if (isatty? (current-input-port)) 
      (begin
        (display "Microshaft Employee Database") 
        (newline)
        (run-program db '() #t))
      (run-program db '() #f))))
