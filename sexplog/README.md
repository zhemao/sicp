# Sexplog
## Logic Query Language from Microshaft, Inc.

## Usage

Run the code with the GNU Guile Scheme implementation

    guile -s main.scm microshaft.db

Type in your queries at the prompt.
