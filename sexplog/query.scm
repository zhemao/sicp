(define (all-matches matcher pattern datum frames)
  (if (null? frames) 
    (let ((match (matcher pattern datum '())))
      (if match (list match) '()))
    (filter values
            (map 
              (lambda (frame) (matcher pattern datum frame))
              frames))))

(define (generic-query matcher pattern assertions frames)
  (apply append
    (map (lambda (datum) 
           (all-matches matcher pattern datum frames))
         assertions)))

(define (simple-query pattern assertions frames)
  (generic-query simple-match pattern assertions frames))

(define (lisp-value-match pattern datum frame)
  (let ((expr (substitute pattern frame)))
    (if (eval expr (interaction-environment)) frame #f)))

(define (lisp-value-query pattern assertions frames)
  (generic-query lisp-value-match pattern assertions frames))


(define (and-query patterns rules assertions frames)
  (if (null? patterns) frames
    (and-query (cdr patterns) rules assertions 
               (query (car patterns) rules assertions frames))))

(define (or-query patterns rules assertions frames)
  (if (null? patterns) frames
    (apply append
           (cons frames
                 (map (lambda (pattern) 
                        (query pattern rules assertions frames))
                      patterns)))))

(define (list-and lst)
  (if (null? lst) #t
    (if (car lst) 
      (list-and (cdr lst)) #f)))

(define (list-or lst)
  (if (null? lst) #f
    (if (car lst) #t
      (list-or (cdr lst)))))

(define (and-match patterns datum rules frame)
  (if (null? patterns) frame
    (let ((next-frame (compound-match (car patterns) datum rules frame)))
      (if next-frame 
        (and-match (cdr patterns) datum rules next-frame) #f))))

(define (or-match patterns datum rules frame)
  (if (null? patterns) #f
    (let ((next-frame (compound-match (car patterns) datum rules frame)))
      (if next-frame next-frame
        (or-match (cdr patterns) datum rules frame)))))

(define (not-match pattern datum rules frame)
  (if (compound-match pattern datum rules frame) #f frame))

(define (rules-match pattern datum rules frame)
  (let ((rule (find-rule pattern rules frame)))
    (if rule
      (compound-match (frame->body rule) datum rules frame)
      (simple-match pattern datum frame))))

(define (compound-match pattern datum rules frame)
  (if (list? pattern)
    (case (car pattern)
      ((and) (and-match (cdr pattern) datum rules frame))
      ((or) (or-match (cdr pattern) datum rules frame))
      ((not) (not-match (cadr pattern) datum rules frame))
      (else (rules-match pattern datum rules frame)))
    (simple-match pattern datum frame)))

(define (match-exists? pattern rules assertions frame)
  (if (null? assertions) #f
    (let ((match (compound-match pattern (car assertions) rules frame)))
      (if match #t (match-exists? pattern rules (cdr assertions) frame)))))

(define (not-query pattern rules assertions frames)
  (filter 
    (lambda (frame)
      (not (match-exists? pattern rules assertions frame)))
    frames))

(define (query pattern rules assertions frames)
  (if (list? pattern)
    (case (car pattern)
      ((and) (and-query (cdr pattern) rules assertions frames))
      ((or) (or-query (cdr pattern) rules assertions frames))
      ((not) (not-query (cadr pattern) rules assertions frames))
      ((lisp-value) (lisp-value-query (cdr pattern) assertions frames))
      (else 
        (let ((rules (all-rules pattern rules frames)))
          (if (null? rules) 
            (simple-query pattern assertions frames)
            (let ((bodies (map frame->body rules)))
              (or-query bodies rules assertions frames))))))
    (simple-query pattern assertions frames)))


(define (query-and-substitute pattern rules assertions)
  (let ((results (query pattern rules assertions '())))
    (map (lambda (frame) (substitute pattern frame))
         results)))
