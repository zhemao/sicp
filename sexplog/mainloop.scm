(load "matching.scm")
(load "rules.scm")
(load "query.scm")

(define (read-list port db)
  (let ((expr (read port)))
    (if (eof-object? expr) db
      (read-list port (cons expr db)))))

(define (read-database filename)
  (call-with-input-file filename (lambda (port) (read-list port '()))))

(define run-program 
  (lambda (db rules interactive)
    (if interactive (display "> "))
    (let ((expr (read)))
      (cond ((or (eof-object? expr) (eq? expr 'quit)) '())
            ((rule-statement? expr) 
             (run-program db (add-rule expr rules) interactive))
            (else 
              (let ((results (query-and-substitute expr rules db)))
                (for-each (lambda (x) (display x) (newline)) results)
                (run-program db rules interactive)))))))
