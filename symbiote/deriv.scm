(define (deriv-variable var wrt)
  (if (eq? var wrt) 1 0))

(define (eqsym? x y)
  (and (symbol? x) (symbol? y) (eq? x y)))

(define (deriv-expt expr wrt)
  (let ((base (cadr expr))
        (exponent (caddr expr)))
    (cond ((and (eqsym? base wrt) (number? exponent))
           (list '* exponent (list '^ base (- exponent 1))))
          ((eqsym? base 'e) (list '* (derivative exponent wrt) expr))
          ((number? base) 
           (list '* (log base) (derivative exponent wrt) expr)))))

(define (deriv-sum expr wrt)
  (cond 
    ((null? expr) '())
    ((or (eq? '+ (car expr)) (eq? '- (car expr))) 
     (cons (car expr) (deriv-sum (cdr expr) wrt)))
    (else 
      (cons 
        (derivative (car expr) wrt)
        (deriv-sum (cdr expr) wrt)))))


(define (deriv-product expr wrt)
  (let ((a (cadr expr))
        (b (if (null? (cdddr expr)) 
             (caddr expr)
             (cons '* (cddr expr)))))
    (list '+ 
          (list '* a (derivative b wrt)) 
          (list '* (derivative a wrt) b))))

(define (deriv-list expr wrt)
  (cond
    ((or (eq? '+ (car expr)) (eq? '- (car expr))) (deriv-sum expr wrt))
    ((eq? '* (car expr)) (deriv-product expr wrt))
    ((eq? '^ (car expr)) (deriv-expt expr wrt))
    (else (error "Unrecognized operator " (car expr)))))

(define (derivative expr wrt)
  (let ((simpexpr (simplify expr)))
    (cond 
      ((symbol? simpexpr) (deriv-variable simpexpr wrt))
      ((number? simpexpr) 0)
      ((list? simpexpr) (simplify (deriv-list simpexpr wrt)))
      (else (error "Don't know how to derive" simpexpr)))))
