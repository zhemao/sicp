# Symbiote: Symbolic Equation Solver in Scheme

This repo implements a rudimentary symbolic equation solver,
which can simplify and differentiate symbolic expressions.

## Usage:

Start your Scheme repl and type

	(load "simplify.scm")
	(load "deriv.scm")

To simplify an expression

	(simplify '(+ 3 2 (* 5 6 x))) => (+ 5 (* 30 x))

To differentiate an expression with respect to x

	(derivative '(+ (* 3 (^ x 2)) (* 2 x)) 'x) => (+ (* 6 x) 2)

Currently, simplification and differentiation has been implemented for 
addition (+), subtraction (-), multiplication (\*), and exponentiation (^).
