#!/usr/bin/guile -s
!#

(load "simplify.scm")
(load "deriv.scm")

(define (differentiation? expr)
  (and (list? expr) (symbol? (car expr))
       (> (string-length (symbol->string (car expr))) 2)
       (equal? "d/"
               (substring 
                 (symbol->string (car expr))
                 0 2))))

(define (diff-variable expr)
  (string->symbol
    (substring
      (symbol->string (car expr)) 3)))

(define (interpret expr)
  (if (differentiation? expr)
    (derivative (cadr expr) (diff-variable expr))
    (simplify expr)))

(define (run-program)
  (begin 
    (display "> ")
    (let ((expr (read)))
      (if (or (eof-object? expr) 
              (eq? 'quit expr)) 
        '()
        (begin 
          (display (interpret expr))
          (newline)
          (run-program))))))

(if (not (equal? "guile" (car (command-line))))
  (run-program))
